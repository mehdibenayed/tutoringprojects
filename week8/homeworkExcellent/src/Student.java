public class Student {
    private String name;
    private int age;
    private int id;
    private Project project;

    public Student(int id,String name, int age) {
        this.id=id;
        this.name = name;
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public int getId() {
        return id;
    }

    public Project getProject() {
        return project;
    }

    @Override
    public String toString() {
        if (project ==null){
            return "Student{" +
                    "id='" + id + '\'' +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
        return "Student{" +
                "id='" + id + '\'' +
                "name='" + name + '\'' +
                ", age=" + age +
                ", project=" + project +
                '}';
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String getName() {
        return name;
    }

    public static int ageComparator(Student s1, Student s2) {
        return Integer.compare(s1.getAge(),s2.getAge());
    }

    public static int ageComparator(Object s1, Object s2) {
        return Integer.compare(((Student) s1).getAge(), ((Student) s2).getAge());
    }
}
