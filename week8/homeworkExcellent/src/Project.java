public class Project {
    private String projectName;
    private int ID;

    public Project(String projectName, int ID) {
        this.projectName = projectName;
        this.ID = ID;
    }

    @Override
    public String toString() {
        return "Project{" +
                "projectName='" + projectName + '\'' +
                ", ID=" + ID +
                '}';
    }

    public int getID() {
        return ID;
    }
}
