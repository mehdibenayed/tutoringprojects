import java.util.ArrayList;
import java.util.Comparator;

public class Storage<T> {
    ArrayList<T> storage;

    public Storage() {
        this.storage = new ArrayList<>();
    }

    public void add(T item){
        storage.add(item);
    }

    public T get(int index){
        return storage.get(index);
    }

    public int size(){
        return storage.size();
    }

    public void sort(Comparator comparator){
        storage.sort(comparator);

    }
}
