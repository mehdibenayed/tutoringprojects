import java.util.ArrayList;
import java.util.Scanner;

public class Controller {

    private Storage<Student> students;
    private Storage<Project> projects;
    private ProjectFactory projectFactory;
    private StudentFactory studentFactory;

    public Controller() {
        students = new Storage<>();
        projects= new Storage<>();
        projectFactory = new ProjectFactory();
        studentFactory = new StudentFactory();
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);
        boolean stay = true;
        int choice;
        System.out.println("Project started");
        while (stay){
            System.out.println("MENU: Please choose an option:");
            System.out.println("0:end / 1:add student / 2:add project / 3:print students / 4:print projects / 5:assign project / 6:sort");
            System.out.print("#Choice: ");
            choice= scanner.nextInt();
            scanner.nextLine();
            switch (choice){
                case 0: stay=false;break;
                case 1: addStudent(scanner); break;
                case 2: addProject(scanner);break;
                case 3: printStudents();break;
                case 4: printProjects();break;
                case 5: assignProjectToStudent(scanner);break;
                case 6: sortStudents();break;
            }
            if (AllStudentsHasProjects() && students.size()!=0){
                stay= false;
                printStudents();
                System.out.println("All student has projects.");
            }
        }
    }

    private boolean AllStudentsHasProjects() {
        for (int i =0; i< students.size(); i++){
            if (students.get(i).getProject() == null){
                return false;
            }
        }
        return true;
    }

    private void sortStudents() {
        students.sort(Student :: ageComparator);
        //students.sort( Student ::getName   );
        System.out.println("Students are sorted!");
    }


    private void addStudent(Scanner scanner) {
        System.out.println("ADDING STUDENT");
        System.out.print("#Please enter the name of the student: ");
        String name= scanner.nextLine();
        System.out.print("#Please enter the age of the student: ");
        int age = scanner.nextInt();
        scanner.nextLine();
        Student found = findStudentByName(name);
        if (found == null){
            students.add(studentFactory.createNewStudent(name,age));
            System.out.println("Student added successfully.");
        }else {
            System.out.println("Student name already exists! ");
        }
    }
    private void addProject(Scanner scanner){
        System.out.println("ADDING A PROJECT:");
        System.out.print("#please enter project name: ");
        String name = scanner.nextLine();
        projects.add(projectFactory.createNewProject(name));
        System.out.println("Project added successfully!");
    }
    private void printStudents(){
        System.out.println("PRINTING STUDENTS:");
        if (students.size() == 0){
            System.out.println("No students added!");
        }
        for (int i = 0 ; i<students.size(); i++){
            System.out.format("%d) %s",i+1,students.get(i));
            System.out.println();
        }
    }
    private void printProjects(){
        System.out.println("PRINTING PROJECTS: ");
        if (projects.size()==0){
            System.out.println("No projects added.");
        }
        for (int i = 0 ; i < projects.size(); i++){
            System.out.format("%d) %s", i+1 , projects.get(i));
            System.out.println();
        }
    }
    private void assignProjectToStudent(Scanner scanner) {
        if (students.size()==0|| projects.size()==0){
            System.out.println("Please enter students and projects first!");
        }else {
            System.out.println("ASSIGNING PROJECT TO STUDENT:");
            printStudents();
            System.out.print("#Please enter a name from the list above: ");
            String name = scanner.nextLine();
            Student student = findStudentByName(name);
            if (student != null){
                printProjects();
                System.out.print("#Please enter an ID from the list above: ");
                int id = scanner.nextInt();
                scanner.nextLine();
                Project project = findProjectByID(id);
                if (project != null){
                    student.setProject(project);
                    System.out.println("Project was assigned to student!");
                }else {
                    System.out.println("Project ID doesn't exist!");
                }
            }else{
                System.out.println("Student name doesn't exist!");
            }
        }

    }

    private Student findStudentByName(String name) {
        for (int i =0; i< students.size(); i++){
            if (students.get(i).getName().toLowerCase().equals(name.toLowerCase())){
                return students.get(i);
            }
        }
        return null;
    }
    private Project findProjectByID(int id) {
        for (int i =0; i< projects.size(); i++){
            if (projects.get(i).getID() == id){
                return projects.get(i);
            }
        }
        return null;
    }
}
