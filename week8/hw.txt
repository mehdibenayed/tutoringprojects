Redo the homework from 2 weeks ago: 
Assigning projects to students.

Create a console application:
*Create a menu in which we can:
0) end the program
1) add students
2) add projects
3) print students
4) print projects
5) assign a project to a student
6) sort students (by name or by age)

*Each project has a name and an ID generated automatically
*Each student has a name and an age and an ID generated automatically
*The student name should be unique (check if name exists before creating him/her)
*Choose a student by his/her name and then assign to him/her a project by choosing a project ID.
*When printing a student, show the student name, student age and the assigned project. If the student has no project yet, only show the student name and the age ( toString() )
*Sort the students based on their names(ascendent) or based on the age(descendent)
*if all students received a project, the program terminates itself.

//Additions:
create a generic class "storage" that contains:(we will use it to store student and store projects)
-ArrayList<T>
-constractor
-add(T)
-get(int)
-size()
-remove(int)
-sort(Comparator)

*in Controller class, students and projects should be stored in instances of Storage class.
*in Controller class, make all the changes required to convert from ArrayList to Stroage
*create 2 Comparators in Student class, one for the name and another for age, and use these comparators as a parameter for sort()

You can find the previous solution here: https://gitlab.com/mehdibenayed/gitpractice
It's okey if you find issues with during work (especially with Comparators) it's fine, just do your best :) 

