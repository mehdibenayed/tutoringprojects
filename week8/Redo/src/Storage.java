import java.util.ArrayList;
import java.util.Comparator;

public class Storage <T extends Person>{
    ArrayList<T> persons;

    public Storage() {
        this.persons = new ArrayList<>();
    }

    public void add(T item){
        persons.add(item);
    }

    public T get(int index){
        return persons.get(index);
    }

    public int size(){
        return persons.size();
    }

    public void sort1(){
        persons.sort(
                (p1, p2) -> p1.getLastName().compareToIgnoreCase(p2.getLastName())
        );
    }

    public void sort2(){
        persons.sort(Comparator.comparing(Person :: getFirstName));
    }

    public void sort3(){
        persons.sort(Person :: specialComparator);

    }

    public void sort4(){
        persons.sort(Person :: birthYearComparator);
    }

}
