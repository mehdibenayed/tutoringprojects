public class Person {

    private String firstName;
    private String lastName;
    private int birthYear;

    public Person(String firstName, String lastName, int birthYear) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthYear = birthYear;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getBirthYear() {
        return birthYear;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthYear=" + birthYear +
                '}';
    }

    public static <T extends Person> int birthYearComparator(T p1, T p2) {
        return Integer.compare(p2.getBirthYear(), p1.getBirthYear());
    }

    public static <T extends Person> int specialComparator(T p1, T p2) {
        int c = p1.getLastName().compareToIgnoreCase(p2.getLastName());
        if(c==0) return p1.getFirstName().compareToIgnoreCase(p2.getFirstName());
        return c;
    }
}
