import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        //Create Persons
        Person person1 = new Person("John", "Doe", 1980);
        Person person2 = new Person("John", "Smith", 1982);
        Person person3 = new Person("Jane", "DOe", 1985);

        //Create ArrayList<Person>
        Storage<Person> storage = new Storage<>();

        //Add storage to ArrayList
        storage.add(person1);
        storage.add(person2);
        storage.add(person3);

        //Print the ArrayList
        print("original", storage);


        //sort the ArrayList

        storage.sort1();
        print(" sorted by last name", storage);

        storage.sort2();
        print(" sorted by first name", storage);


        storage.sort3();
        print(" sorted by last then first name", storage);


        storage.sort4();
        print(" sorted by birth year descending", storage);
    }

    private static void print(String msg, Storage<Person> persons) {
        System.out.println();
        System.out.printf(">>> persons: %s%n", msg);
        for (int i =0 ; i<persons.size()  ; i++){
            System.out.println(persons.get(i));
        }
    }
}
