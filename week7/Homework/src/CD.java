public class CD {
    protected int id;
    protected String title;
    protected boolean isFree;

    public CD(int id, String title) {
        this.id = id;
        this.title = title;
        this.isFree = true;
    }

    public boolean isFree() {
        return isFree;
    }

    public int getId() {
        return id;
    }

    public void setFree(boolean free) {
        isFree = free;
    }

    @Override
    public String toString() {
        return "CD{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", isFree=" + isFree +
                '}';
    }
}
