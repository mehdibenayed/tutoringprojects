public class Series  extends  CD{
    private int episodesCount;

    public Series(int id, String title, int episodesCount) {
        super(id, title);
        this.episodesCount = episodesCount;
    }

    @Override
    public String toString() {
        return "Series{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", episodesCount=" + episodesCount +
                ", isFree=" + isFree +
                '}';
    }
}
