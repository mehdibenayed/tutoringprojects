public class Movie extends CD{

    private int duration;

    public Movie(int id, String title, int duration) {
        super(id, title);
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", duration=" + duration +
                ", isFree=" + isFree +
                '}';
    }
}
