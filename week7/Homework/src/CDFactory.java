public class CDFactory {
    private int numberOfCDsCreated;

    public CDFactory() {
        this.numberOfCDsCreated = 0;
    }
    public Movie createMovie(String title, int duration){
        numberOfCDsCreated++;
        return new Movie(numberOfCDsCreated, title,duration);
    }
    public Series createSeries(String title, int episodesCount){
        numberOfCDsCreated++;
        return new Series(numberOfCDsCreated, title,episodesCount);
    }
}
