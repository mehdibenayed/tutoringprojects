import java.util.ArrayList;
import java.util.Scanner;

public class Store {

    private ArrayList<CD> cds;
    private CDFactory factory;

    public Store() {
        cds= new ArrayList<>();
        factory = new CDFactory();
    }

    public void start(){
        Scanner scanner = new Scanner(System.in);
        boolean stay = true;
        int choice;
        System.out.println("The store is open");
        while (stay){
            System.out.println("MENU: Please choose an option:");
            System.out.println("0:end / 1:add movie / 2:add serie / 3:print CDs / 4:borrow a CD / 5:return a CD");
            System.out.print("#Choice: ");
            choice= scanner.nextInt();
            scanner.nextLine();
            switch (choice){
                case 0: stay=false;break;
                case 1: addMovie(scanner); break;
                case 2: addSeries(scanner);break;
                case 3: printAvailableCDs();break;
                case 4: borrowCD(scanner);break;
                case 5: returnCD(scanner);break;
            }
        }
    }



    private void addMovie(Scanner scanner) {
        System.out.println("ADDING Movie");
        System.out.print("#Please enter the name of the movie: ");
        String name= scanner.nextLine();
        System.out.print("#Please enter the duration of the movie: ");
        int duration = scanner.nextInt();
        scanner.nextLine();
        cds.add(factory.createMovie(name,duration));
        System.out.println("Movie added successfully.");
    }
    private void addSeries(Scanner scanner) {
        System.out.println("ADDING Series");
        System.out.print("#Please enter the name of the series: ");
        String name= scanner.nextLine();
        System.out.print("#Please enter the number of episodes of the series: ");
        int episodesCount = scanner.nextInt();
        scanner.nextLine();
        cds.add(factory.createSeries(name,episodesCount));
        System.out.println("Series added successfully.");
    }
    private void printAvailableCDs() {
        System.out.println("Printing available CDs");
        for (CD cd :cds             ) {
            if (cd.isFree()){
                System.out.println(cd);
            }
        }
    }
    private void borrowCD(Scanner scanner) {
        System.out.println("Borrow a CD: ");
        printAvailableCDs();
        System.out.println("Please choose an id of the CD you want to borrow: ");
        System.out.print("#Choice: ");
        int id= scanner.nextInt();
        scanner.nextLine();
        CD found= findCDByID(id);
        if (found !=null){
            found.setFree(false);
            System.out.println("CD borrowed! Please return it after watching!");
        }else {
            System.out.println("ID incorrect!");
        }
    }
    private void returnCD(Scanner scanner) {
        System.out.println("Return a CD:");
        printBorrowedCDs();
        System.out.println("Please choose an id of the CD you want to return: ");
        System.out.print("#Choice: ");
        int id= scanner.nextInt();
        scanner.nextLine();
        CD found= findCDByID(id);
        if (found !=null){
            found.setFree(true);
            System.out.println("CD returned! I hope you enjoyed watching it!");
        }else {
            System.out.println("ID incorrect!");
        }

    }

    private void printBorrowedCDs() {
        System.out.println("Borrowed CDs: ");
        for (CD cd :cds             ) {
            if (!cd.isFree()){
                System.out.println(cd);
            }
        }
    }

    private CD findCDByID(int id) {
        for (CD cd :cds             ) {
            if (cd.getId() == id){
                return cd;
            }
        }
        return null;
    }
}
