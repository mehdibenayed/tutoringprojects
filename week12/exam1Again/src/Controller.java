import java.util.Scanner;

public class Controller {
    //Fields
    private Scanner scanner;
    private Storage localStorage;
    private boolean online;

    //Constractor
    public Controller() {
        localStorage=new Storage();
        scanner= new Scanner(System.in);
        online = false;
    }

    //Methods
    public void start() {
        boolean stay =true;
        int choice;
        do {
            System.out.println("MENU: 1-enter position / 2-list localStorage / 3-delete from localStorage" +
                    "/ 4-connection state / 5-set connection state / 6-exit");
            choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice){
                case 1: createPosition(scanner);break;
                case 2: listLocalStorage();break;
                case 3: removeFromLocalStorage(scanner);break;
                case 4: readConnectionState();break;
                case 5: setConnectionState(scanner);break;
                case 6: stay=false;break;
            }
        }while (stay);
    }
    private void createPosition(Scanner scanner){
        System.out.println("Hey, please enter a position:");
        System.out.print("x: ");
        int x = scanner.nextInt();
        scanner.nextLine();
        System.out.print("y: ");
        int y = scanner.nextInt();
        scanner.nextLine();
        System.out.print("z: ");
        int z = scanner.nextInt();
        scanner.nextLine();

        Position newPosition = new Position(x,y,z);

        if (online){
            System.out.println(newPosition);
        }else{
            localStorage.add(newPosition);
            System.out.format("Connection offline, %d data in local storage",localStorage.size());
            System.out.println();
        }


    }

    private void listLocalStorage() {
        if (localStorage.size() == 0){
            System.out.println("Local storage is empty");
        }else {
            for (int i = 0 ; i< localStorage.size();i++){
                System.out.println(localStorage.get(i));
            }
        }
    }

    private void removeFromLocalStorage(Scanner scanner) {
        System.out.println("How many positions you would like to delete: ");
        int choice = scanner.nextInt();
        scanner.nextLine();

        localStorage.multipleDelete(choice);

    }

    private void readConnectionState() {
        if (online){
            System.out.println("Connection is online");
        }else {
            System.out.println("Connection is offline");
        }
    }

    private void setConnectionState(Scanner scanner) {
        System.out.println("Set connection (online/offline) : ");
        String choice = scanner.nextLine();
        if (choice.equals("online")){
            online = true;
            System.out.println("Connection is online");
            System.out.println("Sync");
            listLocalStorage();
            localStorage.clear();
        }else {
            online= false;
            System.out.println("Connection is offline");
        }
    }
}
