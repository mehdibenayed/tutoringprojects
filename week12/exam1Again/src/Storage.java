import java.util.ArrayList;

public class Storage {
    //Fields
    private ArrayList<Position> storage;

    //Constractor
    public Storage() {
        this.storage = new ArrayList<>();
    }


    //Methods
    public void add(Position newPosition) {
        storage.add(newPosition);
    }


    public int size() {
        return storage.size();
    }

    public Position get(int i) {
        return storage.get(i);
    }

    public void clear() {
        storage.clear();
    }

    public void multipleDelete(int toDelete) {

    }
}
