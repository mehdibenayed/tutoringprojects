package com.company.Task2;

public class JobFactory {
    private int numberOfJobCreated;

    public JobFactory() {
        numberOfJobCreated = 0;
    }

    public PrintJob createNewJob(int priority, String fileName) {
        numberOfJobCreated ++;
        return new PrintJob(numberOfJobCreated,priority,fileName);
    }
}
