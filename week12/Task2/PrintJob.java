package com.company.Task2;

public class PrintJob {
    private int ID;
    private int priority;
    private String fileName;

    public PrintJob(int ID, int priority, String fileName) {
        this.ID = ID;
        this.priority = priority;
        this.fileName = fileName;
    }

    public int getPriority() {
        return priority;
    }

    public int getID() {
        return ID;
    }

    @Override
    public String toString() {
        return "#" + ID ;
    }
}
