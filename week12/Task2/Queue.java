package com.company.Task2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Queue {
    private ArrayList<PrintJob> queue;

    public Queue() {
        queue = new ArrayList<>();
    }

    public void add(PrintJob newJobs) {
        queue.add(newJobs);
    }

    public PrintJob get(int i) {
        return queue.get(i);
    }

    public void sort() {
        Collections.sort(queue, new Comparator<PrintJob>() {
            @Override
            public int compare(PrintJob o1, PrintJob o2) {
                return o1.getPriority() - o2.getPriority();
            }
        });
    }

    @Override
    public String toString() {
        return "" + queue;
    }

    public void remove() {
        queue.remove(0);
    }

    public int size(){
        return queue.size();
    }
}
