package com.company.Task2;

import java.util.*;

public class Controller {

    private Scanner scanner;
    private ArrayList<PrintJob> printJobs;
    private boolean busy;
    private JobFactory jobFactory;
    private Queue queue;

    public Controller() {
        scanner = new Scanner(System.in);
        printJobs = new ArrayList<>();
        busy = false;
        jobFactory = new JobFactory();
        queue = new Queue();
    }

    public void start() {
        boolean stay = true;
        int choice;
        System.out.println("no printing, queue is empty, printer is ready to print!");
        do {
            System.out.println("MENU: 1-add new print job / 2-list queue / 3-set ready / 4-exit");
            choice = scanner.nextInt();
            scanner.nextLine();
            switch (choice) {
                case 1:
                    addPrintJob(); break;
                case 2:
                    queue.sort(); listJobs(); break;
                case 3:
                    setState(); break;
                case 4:
                    stay = false; break;
                default:
                    System.out.println("Unvalid input!");
            }
        } while (stay);
    }

    private void setState() {
        if (printJobs.size() == 0) {
            System.out.println("There are no jobs need to be printed!");
        } else {
            System.out.println(printJobs.get(0) + " has been finished!");
            printJobs.remove(0);
        }

        if (queue.size() == 0) {
            System.out.println("Printer is ready!");
        } else {
            queue.remove();
        }
    }


    private void listJobs() {

//        Collections.sort(printJobs, new Comparator<PrintJob>() {
//            @Override
//            public int compare(PrintJob o1, PrintJob o2) {
//                return o1.getPriority() - o2.getPriority();
//            }
//        });

        System.out.println(queue);
    }

    private void addPrintJob() {
        System.out.println("Please enter the file name with path!");
        String fileName = scanner.nextLine();

        System.out.println("Please enter the priority of the job!");
        int priority = scanner.nextInt();

        printJobs.add(jobFactory.createNewJob(priority,fileName));

        if (busy) {
            System.out.println("No new printing, printer is still busy with" + printJobs.get(0));
            queue.add(printJobs.get(printJobs.size()-1));
        } else {
            busy = true;
        }
    }
}
