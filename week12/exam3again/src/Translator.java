import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.PrimitiveIterator;

public class Translator {
    //
    private String fileName;
    private ArrayList<String> keys;

    //
    public Translator(String fileName) {
        this.fileName=fileName;
        this.keys = new ArrayList<>();
        loadKeys();
    }

    private void loadKeys() {
        keys.add("menu");
        keys.add("choice");
        keys.add("address");
        keys.add("comfort");
        keys.add("area");
        keys.add("price");
        keys.add("addSucc");
        keys.add("available");
        keys.add("delete");
        keys.add("deleteSucc");
        keys.add("idError");
        keys.add("budget");
        keys.add("availableBudget");

    }

    public Dictionary getDictionary() {

        Dictionary dictionary = new Hashtable();
        ArrayList<String> values = FileLoader.getLines(fileName);
        for (int i =0 ; i<keys.size();i++){
            dictionary.put(keys.get(i), values.get(i));
        }
        return dictionary;
    }

    //

}
