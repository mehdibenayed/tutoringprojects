public class FlatFactory {
    private int numberOfFlatsCreated;

    public FlatFactory(){
        this.numberOfFlatsCreated= 0;
    }

    public Flat createNewFlat(String address,int comfort, int area, int price){
        numberOfFlatsCreated++;
        return new Flat(numberOfFlatsCreated, address,comfort,area,price);
    }
}
