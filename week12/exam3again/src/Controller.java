import java.util.Dictionary;
import java.util.Scanner;

public class Controller {
    Scanner scanner;

    public Controller() {
        scanner = new Scanner(System.in);
    }

    public void start() {
        System.out.print("Input the file name: ");
        String fileName =scanner.nextLine();
        Translator translator = new Translator(fileName);
        Dictionary dictionary = translator.getDictionary();
        Menu menu = new Menu(dictionary);
        menu.show();
    }
}
