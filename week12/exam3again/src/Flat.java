public class Flat {
    //
    private int id;
    private String address;
    private int comfort;
    private int area;
    private int cost;

    public Flat(int id, String address, int comfort, int area, int cost) {
        this.id = id;
        this.address = address;
        this.comfort = comfort;
        this.area = area;
        this.cost = cost;
    }

    public int getId() {
        return id;
    }

    public int getCost() {
        return cost;
    }

    @Override
    public String toString() {
        return "Flat{" +
                "id=" + id +
                ", address='" + address + '\'' +
                ", comfort=" + comfort +
                ", area=" + area +"m²"+
                ", cost=" + cost +" euro"+
                '}';
    }
}
