import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class FileLoader {

    public static ArrayList<String> getLines(String fileName) {
        ArrayList<String> lines = new ArrayList<>();

        try {
            Scanner scanner = new Scanner(new File(fileName));
            String line;
            while (scanner.hasNextLine()){
                line = scanner.nextLine();
                lines.add(line);
            }



        }catch (IOException e){
            System.out.println(e);
        }

        return lines;
    }
}
