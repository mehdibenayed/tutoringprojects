import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class FileOutputChannel implements  IOutputChannel{
    private String filePathAndName;

    public FileOutputChannel(String filePathAndName) {
        this.filePathAndName = filePathAndName;
    }

    public String getFilePathAndName() {
        return filePathAndName;
    }

    public void setFilePathAndName(String filePathAndName) {
        this.filePathAndName = filePathAndName;
    }

    @Override
    public void sendMessage(LogMessage message) throws IOException {
        FileWriter file = new FileWriter(filePathAndName, true);

        DateFormat formatter = new SimpleDateFormat("yyyy.MM.dd HH:mm");
        file.write(String.format("%s [L%d] %s\n",
                formatter.format(message.getSendDate()),
                message.getLevel(),
                message.getMessage())
        );
        file.close();
    }
}
