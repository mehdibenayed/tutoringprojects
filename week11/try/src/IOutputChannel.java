import java.io.IOException;

public interface IOutputChannel {
    void sendMessage(LogMessage message) throws IOException;
}
