import java.util.Date;

public class LogMessage {
    //
    private Date sendDate;
    private int level;
    private String message;
    //
    public Date getSendDate() {
        return sendDate;
    }

    public int getLevel() {
        return level;
    }

    public String getMessage() {
        return message;
    }

    //Constructor
    public LogMessage(Date sendDate, int level, String message) {
        this.sendDate = sendDate;
        this.level = level;
        this.message = message;
    }
}
