import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class Logger {
    private int logLevel;
    private ArrayList<IOutputChannel> outputChannels;

    public Logger() {
        this.logLevel = 5;
        this.outputChannels = new ArrayList<>();
    }

    public int getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(int logLevel) {
        this.logLevel = logLevel;
    }

    public void addChannel(IOutputChannel newChannel){
        outputChannels.add(newChannel);
    }

    public void write(String message, int messageLogLevel) throws IOException {
        LogMessage newMessage = new LogMessage(
                new Date(),
                messageLogLevel,
                message
        );

        if(messageLogLevel <= logLevel) {
            sendMessage(newMessage);
        }
    }

    protected void sendMessage(LogMessage message) throws IOException {
        for (IOutputChannel channel: outputChannels) {
            channel.sendMessage(message);
        }
    }

}
