public class SingletonNetworkLogger extends NetworkLogger {

    private static SingletonNetworkLogger sharedInstance;

    private SingletonNetworkLogger() {
        super();
    }

    public static SingletonNetworkLogger getSharedInstance() {
        if(sharedInstance == null) {
            sharedInstance = new SingletonNetworkLogger();
        }
        return sharedInstance;
    }

}
