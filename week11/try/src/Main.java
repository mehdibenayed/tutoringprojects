import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try {

            SingletonNetworkLogger.getSharedInstance().setLogLevel(5);
            SingletonNetworkLogger.getSharedInstance().addChannel(new FileOutputChannel("Events.log"));

            SingletonNetworkLogger.getSharedInstance().write("App started", 4);
            SingletonNetworkLogger.getSharedInstance().write("App ended", 5);

        }catch (IOException e){
            System.out.println(e);
        }

    }
}
