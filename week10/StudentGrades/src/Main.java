import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        ArrayList<Student> students = StudentLoader.loadfromTextFile("src\\students.txt");
        print(students);
        assignGradesToStudents(students);
        print(students);
        StudentWriter.writeToTextFile("src\\studentsGrades.txt", students);
        System.out.println("txt file was created and updated!");

        StudentWriter.writeToJSONFile("src\\studentsGrades.json", students);
        System.out.println("json file was created and updated!");


        ArrayList<Student> newStudents = new ArrayList<>();
        try {
            newStudents = StudentLoader.loadFromJSONFile("src\\studentsGrades.json");
        }catch (IOException e){
            System.out.println(e);
        }
        print(newStudents);
    }

    private static void assignGradesToStudents(ArrayList<Student> students) {
        Scanner scanner = new Scanner(System.in);
        int grade;
        for (Student s :students             ) {
            System.out.println("please assign grade to "+ s);
            grade= scanner.nextInt();
            scanner.nextLine();
            s.setGrade(grade);
        }

    }

    private static void print(ArrayList<Student> students) {
        for (Student s: students             ) {
            System.out.println(s);
        }
    }
}
