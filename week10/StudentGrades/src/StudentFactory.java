public class StudentFactory {
    private int numberOfStudentsCreated;

    public StudentFactory(){
        numberOfStudentsCreated=0;
    }

    public Student createStudent(String line){
        numberOfStudentsCreated++;
        String[] names = line.split(",");
        return new Student(numberOfStudentsCreated,names[0],names[1]);
    }
}
