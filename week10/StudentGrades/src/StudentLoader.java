import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class StudentLoader {

    public static ArrayList<Student> loadfromTextFile(String fileName) {

        StudentFactory factory = new StudentFactory();
        ArrayList<Student> students = new ArrayList<>();
        try{
            Scanner scanner = new Scanner(new File(fileName));
            String line;
            while (scanner.hasNextLine()){
                line = scanner.nextLine();
                students.add(factory.createStudent(line));
            }
        }catch (IOException e){
            System.out.println(e);
        }
        return students;
    }


    public static ArrayList<Student> loadFromJSONFile(String fileName) throws IOException {

        Type listType = new TypeToken<ArrayList<Student>>(){}.getType();

        String jsonString = new String (
                Files.readAllBytes( Paths.get(fileName) )
        );

        Gson gson = new Gson();

        return gson.fromJson(jsonString, listType);


    }
}
