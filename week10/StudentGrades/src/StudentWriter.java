import com.google.gson.Gson;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class StudentWriter {

    public static void writeToTextFile(String fileName, ArrayList<Student> students) {
        try {
            FileWriter writer = new FileWriter(fileName);
            String line;
            for (Student s : students){
                line = s.getFirstName()+","+s.getLastName()+","+s.getGrade()+"\n";
                writer.write(line);
            }
            writer.close();
        }catch (IOException e){
            System.out.println(e);
        }
    }

    public static void writeToJSONFile(String fileName, ArrayList<Student> students)  {
        try {
            Gson gson = new Gson();
            String line= gson.toJson(students);
            FileWriter writer = new FileWriter(fileName);
            writer.write(line);
            writer.close();

        }catch (IOException e) {
            System.out.println(e);
        }

    }
}
