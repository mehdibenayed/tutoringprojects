package com.company;

import java.util.Scanner;

public class Competition {
    //Fields
    private String competitionName;
    private JuryMember[] juryMembers;
    private Participant[] participants;
    //Constructor

    public Competition(String competitionName) {
        this.competitionName = competitionName;
    }


    //Methods

    public void Start(Scanner scanner){
        RecruitJury(scanner);
        RecruitParticipants(scanner);
        evaluateParticipants();
        PrintParticipants();
        SortParticipants();
        PrintParticipants();
    }

    private void SortParticipants() {
        boolean hasChange;
        do {
            hasChange = false;
            for (int i = 0; i < participants.length - 1; i++) {
                if (participants[i].getTotalPoints() < participants[i + 1].getTotalPoints()) {
                    Participant temp;
                    temp = participants[i];
                    participants[i] = participants[i + 1];
                    participants[i + 1] = temp;
                    hasChange = true;
                }
            }
        }while(hasChange);

    }

    private void evaluateParticipants() {
        int points;
        for (int i = 0 ; i< participants.length ; i++){
            for( int j=0 ; j< juryMembers.length ;j++){
                points = juryMembers[j].givePoints();
                participants[i].AddPoints(points);
                System.out.println(participants[i].getParticipantName() + " received "+
                        points + " points from " + juryMembers[j].getJuryName());
            }
        }

    }

    public void RecruitJury(Scanner scanner){
        System.out.println("please enter the nymber of jury members : ");
        int n = scanner.nextInt();
        scanner.nextLine();
        juryMembers = new JuryMember[n];

        for(int i = 0 ; i<n ;i++){
            System.out.println("enter the name of jury");
            juryMembers[i] = new JuryMember(scanner.nextLine());
        }
    }
    public void PrintParticipants(){
        for (int i=0 ; i< participants.length ; i++){
            System.out.println("ID: "+ participants[i].getID() +
                                " ; Name : " +participants[i].getParticipantName()+
                                " ; Age :" +participants[i].getAge()+
                                " ; Score :" +participants[i].getTotalPoints() );
        }
    }

    public void RecruitParticipants(Scanner scanner){
        System.out.println("please enter number of participants : ");
        int n = scanner.nextInt();
        String name;
        int age;
        scanner.nextLine();
        participants = new Participant[n];

        for (int i = 0 ; i<n ;i++){
            System.out.println("enter the name of participant :");
            name= scanner.nextLine();
            System.out.println("enter paticipant age : ");
            age = scanner.nextInt();
            scanner.nextLine();

            participants[i] = ParticipantFactory.CreateParticipant(name,age);
        }
    }
    public String getCompetitionName() {
        return competitionName;
    }
}
