package com.company;

import jdk.jshell.MethodSnippet;

public class ParticipantFactory {
    //Fields
    private static int numberOfParticipantsCreated;

    //Constructor
    public ParticipantFactory(){
        this.numberOfParticipantsCreated = 0;
    }

    //Methods
    public static Participant CreateParticipant(String name, int age){
        numberOfParticipantsCreated++;
        return new Participant(name,age, numberOfParticipantsCreated);

    }
}
