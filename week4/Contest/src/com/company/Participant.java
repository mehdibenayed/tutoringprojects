package com.company;

import java.util.SplittableRandom;

public class Participant {
    //Fields
    private String participantName;
    private int age;
    private int ID;
    private int totalPoints;

    //Constructor

    public Participant(String participantName, int age, int ID) {
        this.participantName = participantName;
        this.age = age;
        this.ID = ID;
        this.totalPoints = 0;
    }

    //Methods
    public void AddPoints(int extraPoints){
        totalPoints += extraPoints;
    }

    public String getParticipantName() {
        return participantName;
    }

    public int getAge() {
        return age;
    }

    public int getID() {
        return ID;
    }

    public int getTotalPoints() {
        return totalPoints;
    }
}
