package com.company;

import java.util.Random;

public class JuryMember {
    //Fields
    private static final int MAX_POINTS = 10;
    private String JuryName;

    //Constructor

    public JuryMember(String juryName) {
        JuryName = juryName;
    }

    //Methods
    public int givePoints(){
        Random rand = new Random();
        return rand.nextInt(MAX_POINTS)+1;
    }

    public String getJuryName() {
        return JuryName;
    }

}
