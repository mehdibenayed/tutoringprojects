package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("please enter the name of the new competition :");
        String input = scanner.nextLine();

        Competition ourCompetition = new Competition(input);

        ourCompetition.Start(scanner);


    }
}
