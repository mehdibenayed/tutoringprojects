package com.company;

public class Student {
    //
    private int studentAge;
    private String studentName;
    private Course[] studentCourses;
    private int numberOfCourses;

    //

    public Student(int studentAge, String studentName) {
        this.studentAge = studentAge;
        this.studentName = studentName;
        studentCourses = new Course[3];
        numberOfCourses = 0;
    }
    //
    public String getStudentName() {
        return studentName;
    }

    public Course[] getStudentCourses() {
        return studentCourses;
    }

    public void AddCourseToStudent(Course takenCourse){
        studentCourses[numberOfCourses] = takenCourse;
        numberOfCourses++;
    }
}

