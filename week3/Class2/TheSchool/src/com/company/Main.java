package com.company;

public class Main {

    public static void main(String[] args) {

        School ourSchool = new School("SuperSchool");

        Course course1 = new Course("IT",new Teacher("Dr. House"));
        Course course2 = new Course("Math",new Teacher("Dr. White"));
        Course course3 = new Course("English",new Teacher("Dr. Brown"));

        ourSchool.AddCourse(course1);
        ourSchool.AddCourse(course2);
        ourSchool.AddCourse(course3);

        for (int i = 0 ; i<3 ;i++){
            System.out.println(ourSchool.getCourses()[i].getCourseName());
        }

        Student student1 = new Student(20, "Adam");
        Student student2 = new Student(24, "Lara");
        Student student3 = new Student(32, "Joe");
        Student student4 = new Student(18, "Alex");

        ourSchool.AddStuden(student1);
        ourSchool.AddStuden(student2);
        ourSchool.AddStuden(student3);
        ourSchool.AddStuden(student4);

        for (int i = 0 ; i<4 ;i++){
            System.out.println(ourSchool.getStudents()[i].getStudentName());
        }

        ourSchool.getStudents()[0].AddCourseToStudent(course1);
        ourSchool.getStudents()[0].AddCourseToStudent(course2);
        ourSchool.getStudents()[0].AddCourseToStudent(course3);

        ourSchool.getStudents()[1].AddCourseToStudent(course1);
        ourSchool.getStudents()[2].AddCourseToStudent(course3);
        ourSchool.getStudents()[3].AddCourseToStudent(course2);

        for (int i = 0 ; i<4 ;i++){

            for (int j =0; j<3 ;j++){
                if (ourSchool.getStudents()[i].getStudentCourses()[j] != null){
                    System.out.println(
                            ourSchool.getStudents()[i].getStudentName()+ " is studying  "+
                                    ourSchool.getStudents()[i].getStudentCourses()[j].getCourseName()
                    );
                }

            }
        }
    }
}
