package com.company;

public class School {

    //Fields
    private Course[] courses ;
    private Student[] students;
    private String schoolName;
    private int countCourses;
    private int countStudents;


    //Constructor
    public School(String name){
        this.schoolName = name;
        students = new Student[4];
        courses = new Course[3];
        countCourses=0 ;
        countStudents=0;
    }
    //Methods

    public Student[] getStudents() {
        return students;
    }

    public Course[] getCourses() {
        return courses;
    }

    public int getCount() {
        return countCourses;
    }

    public void AddCourse(Course courseToAdd){
        courses[countCourses] = courseToAdd;
        countCourses++;
    }

    public void AddStuden( Student studentToAdd){
        students[countStudents] = studentToAdd;
        countStudents++;
    }

}
