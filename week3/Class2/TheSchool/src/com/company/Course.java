package com.company;

public class Course {
    //Fields
    private String courseName;
    Teacher teacher;

    //Constractor

    public Course(String courseName, Teacher teacher) {
        this.courseName = courseName;
        this.teacher = teacher;
    }
    //Methods


    public String getCourseName() {
        return courseName;
    }
}
