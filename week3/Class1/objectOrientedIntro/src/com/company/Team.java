package com.company;

public class Team {

    //Fields
    Student[] students;
    float avg;

    //Constructor
    public Team(){
        students= new Student[3];
        students[0]= new Student("Mark", 5);
        students[1]= new Student("Sara", 4);
        students[2]= new Student("Alex", 2);
        this.avg = CalculateAverage();
    }
    //Methods
    private float CalculateAverage(){
        float sum=0;
        for(int i = 0 ; i< 3 ; i++){
            sum = sum+ students[i].grade;
        }
        return sum/3;
    }
}
