package com.company;

public class Document {

    //Fiels
    private String title;

    //Constructor
    public Document() {
        title = "ABC";
    }

    //Methods

    public String getTitle() {
        return title;
    }

    public void setTitle(String newTitle) {
        if (newTitle.length()>5){
            this.title = newTitle;
        }
    }
}
