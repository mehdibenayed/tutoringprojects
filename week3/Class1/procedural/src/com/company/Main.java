package com.company;

public class Main {

    public static void main(String[] args) {
        int grades[]= new int[3];
        grades[0]=5;
        grades[1]=4;
        grades[2]=2;

        for (int i = 0 ; i<3 ; i++){
            System.out.println(grades[i]);
        }

        float avg = 0;
        float sum =0;
        for (int i = 0 ; i<3 ;i++){
            sum = sum + grades[i];
        }
        avg = sum/3;

        System.out.println(avg);

    }
}
