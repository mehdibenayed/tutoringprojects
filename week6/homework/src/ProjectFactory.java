public class ProjectFactory {
    private int numberOfProjectsCreated;

    public ProjectFactory() {
        numberOfProjectsCreated=0;
    }
    public Project createNewProject(String name){
        numberOfProjectsCreated++;
        return new Project(name,numberOfProjectsCreated);
    }
}
