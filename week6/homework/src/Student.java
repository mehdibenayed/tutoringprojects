public class Student {
    private String name;
    private int age;
    private Project project;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        if (project ==null){
            return "Student{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", project=" + project +
                '}';
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String getName() {
        return name;
    }
}
