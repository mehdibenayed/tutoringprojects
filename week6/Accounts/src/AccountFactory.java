import model.Account;
import model.Admin;
import model.Customer;

public class AccountFactory {

    private int numberOfAccountsCreated;

    public AccountFactory(){
        this.numberOfAccountsCreated = 0;
    }
    public Admin createAdmin(String userName, String password){
        numberOfAccountsCreated++;
        return new Admin(numberOfAccountsCreated,userName,password);
    }

    public Customer createCustomer(String username, String password) {
        numberOfAccountsCreated++;
        return new Customer(numberOfAccountsCreated,username,password);
    }
}
