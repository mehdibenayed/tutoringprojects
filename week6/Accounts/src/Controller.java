import model.Account;
import model.Admin;

import java.util.ArrayList;
import java.util.Scanner;

public class Controller {

    private ArrayList<Account> accounts;
    private AccountFactory factory;
    private Account logedIn;

    public Controller() {
        this.accounts = new ArrayList<>();
        this.factory= new AccountFactory();
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);
        createFirstAdmin(scanner);
        displayMenu(scanner);
    }

    private void displayMenu(Scanner scanner) {
        boolean stay = true;
        int choice;
        while (stay){

            System.out.println();
            printLogedIn();

            System.out.println("MENU: Please choose an option: ");
            System.out.println("0:Quit / 1:Print / 2:create an account/ 3:delete account / 4:Login / 5:Logout");
            System.out.print("#Choice: ");
            choice = scanner.nextInt();
            scanner.nextLine();
            switch (choice){
                case 0 : stay = false; break;
                case 1 : printAllAccounts(); break;
                case 2 : addAcount(scanner); break;
                case 3 : deleteAccount(scanner); break;
                case 4 : login(scanner); break;
                case 5 : logedIn = null; break;

                default:
                    System.out.println("ERROR: Choice invalid! "); break;
            }
        }
    }

    private void deleteAccount(Scanner scanner) {
        if (logedIn instanceof Admin){
            printAllAccounts();
            System.out.println("Please choose the id of the account you would like to delete");
            System.out.print("#Choice: ");
            int choice = scanner.nextInt();
            scanner.nextLine();
            int found = findAccountIndexByID(choice);
            if (found != -1){
                accounts.remove(found);
                System.out.println("Account removed! ");
            }else {
                System.out.println("ERROR: Account ID doesn't exist! Try again");
            }
        }else {
            System.out.println("ERROR: You should login as admin first! Try again");
        }
    }

    private int findAccountIndexByID(int choice) {
        for (int i =0 ;i< accounts.size(); i++){
            if (accounts.get(i).getId() == choice){
                return i;
            }
        }
        return -1;
    }

    private void login(Scanner scanner) {
        System.out.print("#Please enter your username: ");
        String username = scanner.nextLine();
        Account found = findAccountByUsername(username);
        if (found != null){
            System.out.print("#Please enter your password: ");
            String password = scanner.nextLine();
            if (found.passwordVerified(password)){
                System.out.println("You are now logged in as "+found.getUserName());
                logedIn = found;
            }else {
                System.out.println("ERROR: Password incorrect! Try to login again");
            }
        }else {
            logedIn= null;
            System.out.println("ERROR: Username incorrect! Try to login again");
        }
    }

    private Account findAccountByUsername(String username) {
        for (Account a :accounts             ) {
            if (a.getUserName().equals(username)){
                return a;
            }
        }
        return null;
    }

    private void printLogedIn() {
        System.out.print("Open account: ");
        if (logedIn == null){
            System.out.println("None");
        }else{
            System.out.println(logedIn.getUserName());
        }
    }

    private void addAcount(Scanner scanner) {
        boolean issue = false;
        String username, password;
        int choice;
        Account found;
        do {
            System.out.println("What type of account you would like to create?");
            System.out.println( "0:Back to menu / 1:Admin / 2:Customer");
            System.out.print("#Choice: ");
            choice = scanner.nextInt();
            scanner.nextLine();
            switch (choice){
                case 0 : break;
                case 1 : issue = creatNewAdmin(scanner); break;
                case 2 : issue = createAccount(scanner , "customer"); break;
                default:
                    System.out.println("ERROR! Invalid choice! Please choose again"); issue = true; break;
            }

        }while (issue);

    }

    private boolean creatNewAdmin(Scanner scanner) {
        if (logedIn instanceof Admin){
              return createAccount(scanner, "admin");
        }
        System.out.println("ERROR: You should login as admin first! Try again");
        return true;
    }

    private boolean createAccount(Scanner scanner, String type) {
        String username, password;
        Account found;
        System.out.print("Please enter a username: ");
        username= scanner.nextLine();
        found= findAccountByUsername(username);
        if (found == null){
            System.out.print("Please enter a password: ");
            password = scanner.nextLine();

            if (type.equals("admin")){
                accounts.add(factory.createAdmin(username,password));
                System.out.println("Admin added successfully!");
            }else if (type.equals("customer")){
                accounts.add(factory.createCustomer(username,password));
                System.out.println("Customer added successfully!");
            }

            return false;
        }else {
            System.out.println("ERROR: Username already exist! Please choose another username");
            return true;
        }
    }

    private void printAllAccounts() {
        System.out.println();
        System.out.println("All acounts: ");
        for (Account a:accounts             ) {
            System.out.println(a);
        }
    }

    private void createFirstAdmin(Scanner scanner) {
        System.out.println("Hey new Admin");
        System.out.print("#Please enter a username: ");
        String username = scanner.nextLine();
        System.out.print("#Please enter a password: ");
        String password = scanner.nextLine();
        Admin admin = factory.createAdmin(username,password);
        accounts.add(admin);
        logedIn = admin;
    }
}
