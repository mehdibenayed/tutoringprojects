package model;

public class Customer extends Account {

    public Customer(int id, String userName, String password) {
        super(id, userName, password);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                '}';
    }
}
