package model;

public class Account {
    protected int id;
    protected String userName;
    private String password;

    public Account(int id, String userName, String password) {
        this.id = id;
        this.userName = userName;
        this.password = password;
    }

    @Override
    public String toString() {
        return "model.Account{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                '}';
    }

    public String getUserName() {
        return userName;
    }

    public int getId() {
        return id;
    }

    public boolean passwordVerified(String password) {
        if (password.equals(this.password)){
            return true;
        }
        return false;
    }
}
