package model;

import model.Account;

public class Admin extends Account {

    public Admin(int id, String userName, String password) {
        super(id, userName, password);
    }
    @Override
    public String toString() {
        return "Admin{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                '}';
    }
}
