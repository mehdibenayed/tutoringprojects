public class Student {
    private String name;
    private int age;
    private int id;
    private Project project;

    public Student(int id,String name, int age) {
        this.id=id;
        this.name = name;
        this.age = age;
    }

    public Project getProject() {
        return project;
    }

    @Override
    public String toString() {
        if (project ==null){
            return "Student{" +
                    "id='" + id + '\'' +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
        return "Student{" +
                "id='" + id + '\'' +
                "name='" + name + '\'' +
                ", age=" + age +
                ", project=" + project +
                '}';
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String getName() {
        return name;
    }
}
