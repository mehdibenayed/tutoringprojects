public class StudentFactory {
    private int numberOfStudentsCreated;

    public StudentFactory() {
        numberOfStudentsCreated=0;
    }

    public Student createNewStudent(String name, int age){
        numberOfStudentsCreated++;
        return new Student(numberOfStudentsCreated,name, age);
    }
}
