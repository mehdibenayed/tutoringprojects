package com;

import com.model.Drink;
import com.model.Food;

public class ProductFactory {
    private int numberOfProductsCreated;

    public ProductFactory() {
        numberOfProductsCreated= 0;
    }

    public Food createNewFood(int price, String name){
        numberOfProductsCreated++;
        return new Food(numberOfProductsCreated,price,name);
    }

    public Drink createNewDrink(int price, String type){
        numberOfProductsCreated++;
        return new Drink(numberOfProductsCreated,price,type);
    }
}
