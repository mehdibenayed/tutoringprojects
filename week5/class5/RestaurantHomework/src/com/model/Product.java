package com.model;

public class Product {
    protected int id;
    protected int price;

    public Product(int id, int price) {
        this.id = id;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public int getPrice() {
        return price;
    }
}
