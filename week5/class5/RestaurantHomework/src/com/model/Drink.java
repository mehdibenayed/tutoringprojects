package com.model;

public class Drink extends Product{
    private String drinkType;

    public Drink(int id, int price, String drinkType) {
        super(id, price);
        this.drinkType = drinkType;
    }

    @Override
    public String toString() {
        return "Drink{" +
                "id='" + id + '\'' +
                ", drinkType=" + drinkType +
                ", price=" + price +
                '}';
    }
}
