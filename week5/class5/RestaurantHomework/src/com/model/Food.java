package com.model;

public class Food extends Product {

    private String  foodName;

    public Food(int id, int price, String foodName) {
        super(id, price);
        this.foodName = foodName;
    }

    @Override
    public String toString() {
        return "Food{" +
                "id='" + id + '\'' +
                ", foodName=" + foodName +
                ", price=" + price +
                '}';
    }
}
