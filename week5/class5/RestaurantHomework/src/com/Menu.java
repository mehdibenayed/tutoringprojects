package com;

import com.model.Drink;
import com.model.Food;
import com.model.Product;

import java.util.ArrayList;

public class Menu {
    private ProductFactory factory;
    private ArrayList<Product> products;

    public Menu(){
        products= new ArrayList<>();
        factory = new ProductFactory();
    }

    public void addFood(int price, String name){
        Food foodToCreate = factory.createNewFood(price,name);
        products.add(foodToCreate);
    }

    public void addDrink(int price, String type){
        Drink drinkToCreate = factory.createNewDrink(price,type);
        products.add(drinkToCreate);
    }

    public void sort() {
        boolean hasChange;
        do {
            hasChange = false;
            for (int i = 0; i < products.size() - 1; i++) {
                if (products.get(i).getPrice() < products.get(i+1).getPrice()) {

                    Product temp = products.get(i);
                    products.set(i,products.get(i+1));
                    products.set(i+1,temp);
                    hasChange = true;
                }
            }
        }while(hasChange);
    }

    public void print() {
        for (Product p:products             ) {
            if (p instanceof  Food){
                System.out.println((Food)p);
            }
            if (p instanceof Drink){
                System.out.println((Drink)p);
            }
        }
        System.out.println();
    }

    public Food getFoodByID(int foodID) {
        for (Product p:products             ) {
            if (p instanceof Food && p.getId() == foodID){
                return (Food) p;
            }
        }
        return null;
    }

    public Drink getDrinkByID(int drinkID) {
        for (Product p:products             ) {
            if (p instanceof Drink && p.getId() == drinkID){
                return (Drink) p;
            }
        }
        return null;
    }
}
