package com;

import com.model.Drink;
import com.model.Food;

import java.util.Scanner;

public class Main {

    static Menu menu = new Menu();
    public static void main(String[] args) {


        //Adding products to menu
        addProductToMenu();

        //Sort the menu
        menu.sort();

        //Print menu
        System.out.println("Welcome to our restaurant, this is our menu :");
        menu.print();

        //Get order
        getOrder();

    }

    private static void getOrder() {

        Scanner scanner = new Scanner(System.in);

        //Ask user to chose food
        System.out.print("Please select the ID of the food you want to order (press 0 if none): ");
        int foodID = scanner.nextInt();
        scanner.nextLine();

        //Ask user to chose drink
        System.out.print("Please select the ID of the drink you want to order (press 0 if none): ");
        int drinkID= scanner.nextInt();
        scanner.nextLine();

        System.out.println();

        int foodPrice,drinkPrice;

        foodPrice = prepareFood(foodID);
        drinkPrice = prepareDrink(drinkID);

        //Print total price
        int totalPrice= foodPrice+drinkPrice;
        System.out.format("The total price of your order is: %d forint", totalPrice);
    }

    private static int prepareDrink(int drinkID) {
        Drink drinkSelected = menu.getDrinkByID(drinkID);
        System.out.print("The drink ordered: ");
        if (drinkSelected == null){
            System.out.println("None");
            return  0;
        }
        System.out.println(drinkSelected);
        return drinkSelected.getPrice();
    }

    private static int prepareFood(int foodID) {
        Food foodSelected = menu.getFoodByID(foodID);
        System.out.print("The food ordered: ");
        if (foodSelected == null){
            System.out.println("None");
             return  0;
        }
        System.out.println(foodSelected);
        return foodSelected.getPrice();
    }

    private static void addProductToMenu() {
        menu.addFood(1500, "pizza");
        menu.addFood(1000, "burger");
        menu.addDrink(200, "water");
        menu.addDrink(300, "coke");

    }
}
