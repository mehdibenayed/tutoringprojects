package com.model;

import com.model.Offer;

public class Trip extends Offer {
    //
    private String destination;
    private int duration;

    //

    public Trip(int price, String destination, int duration) {
        super(price);
        this.destination = destination;
        this.duration = duration;
    }

    //

    @Override
    public String toString() {
        return "Trip{" +
                "price=" + price +
                ", destination='" + destination + '\'' +
                ", duration=" + duration +
                '}';
    }

    public int getDuration() {
        return duration;
    }
}
