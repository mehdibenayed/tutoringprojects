package com.model;

import com.model.Offer;

public class Hotel extends Offer {
    //
    private String hotelName;
    private int numberOfStars;

    //

    public Hotel(int price, String hotelName, int numberOfStars) {
        super(price);
        this.hotelName = hotelName;
        this.numberOfStars = numberOfStars;
    }

    //

    @Override
    public String toString() {
        return "Hotel{" +
                "hotelName='" + hotelName + '\'' +
                ", numberOfStars=" + numberOfStars +
                ", price=" + price +
                '}';
    }

    public int getNumberOfStars() {
        return numberOfStars;
    }
}
