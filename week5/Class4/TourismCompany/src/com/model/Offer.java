package com.model;

import java.util.Objects;

public class Offer {
    //

    protected int price;

    //
    public Offer(int price) {
        this.price = price;
    }
    //Methods


    @Override
    public String toString() {
        return "Offer{" +
                "price=" + price +
                '}';
    }

    public int getPrice() {
        return price;
    }
}
