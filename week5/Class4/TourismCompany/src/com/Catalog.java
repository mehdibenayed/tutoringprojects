package com;

import com.model.Hotel;
import com.model.Offer;
import com.model.Trip;

import java.util.ArrayList;

public class Catalog {
    //
    private ArrayList<Offer> offers;

    //

    public Catalog() {
        offers = new ArrayList<>();
    }
    //

    public void addOffer( Offer offer){
        offers.add(offer);
    }

    public void printTrips(){
        for (Offer o:offers             ) {
            if (o instanceof Trip){
                System.out.println(o);
            }
        }
    }
    public void printHotel(){
        for (Offer o:offers             ) {
            if (o instanceof Hotel){
                System.out.println(o);
            }
        }
    }
    public void printAllOffers(){
        System.out.println("All offers available: ");
        printTrips();
        printHotel();
        System.out.println();
    }
    public void printTripsByPrice(int budget){
        System.out.format("Available trips for less than %d euro", budget);
        System.out.println();
        for (Offer o:offers             ) {
            if (o instanceof Trip && o.getPrice() < budget){
                System.out.println(o);
            }
        }
    }
    public void printHotelsByPrice(int budget){
        System.out.format("Available hotels for less than %d euro", budget);
        System.out.println();
        for (Offer o:offers             ) {
            if (o instanceof Hotel && o.getPrice() < budget){
                int numberOfDays = budget/o.getPrice();
                System.out.println(numberOfDays + "days in "+o);
            }
        }
    }
    public void printAllByPrice(int budget){
        printTripsByPrice(budget);
        printHotelsByPrice(budget);
        System.out.println();
    }
    public void printHotelsByStars(int minNumOfStars){
        System.out.format("Available hotels with minimum %d stars", minNumOfStars);
        System.out.println();
        for (Offer o:offers            ) {
            if (o instanceof Hotel && ((Hotel)o).getNumberOfStars() >= minNumOfStars ){
                System.out.println(o);
            }
        }
        System.out.println();
    }
    public void printTripsByDuration(int maxNumOfDays){
        System.out.format("Available trips with maximum %d number of days",maxNumOfDays);
        System.out.println();
        for (Offer o:offers             ) {
            if (o instanceof Trip && ((Trip) o).getDuration() <= maxNumOfDays){
                System.out.println(o);
            }
        }
        System.out.println();
    }


}
