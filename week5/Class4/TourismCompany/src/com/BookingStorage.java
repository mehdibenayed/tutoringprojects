package com;

import com.sun.source.tree.LambdaExpressionTree;

import java.awt.print.Book;
import java.util.ArrayList;

public class BookingStorage {
    //
    private ArrayList<Booking> bookings;

    //

    public BookingStorage() {
        this.bookings = new ArrayList<>();
    }
    //
    public void addBooking(Booking booking){
        bookings.add(booking);
    }

    public void printAllBookings(){
        System.out.println("All bookings made: ");
        for (Booking b:bookings             ) {
            System.out.println(b);
        }
        System.out.println();
    }

    public int totalIncome(){
        int sum = 0;
        for (Booking b : bookings             ) {
            sum+= b.bookingPrice();
        }
        return sum;
    }
}
