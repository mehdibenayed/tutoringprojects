package com;

import com.model.Hotel;
import com.model.Offer;
import com.model.Trip;

import java.awt.print.Book;

public class Booking {
    //
    private Offer offer;
    private int numOfPeople;
    private int numOfNights;

    //

    public Booking(Trip trip, int numOfPeople){
        this.offer = trip;
        this.numOfPeople= numOfPeople;
        this.numOfNights = 1;
    }

    public Booking(Hotel hotel, int numOfPeople, int numOfNights){
        this.offer= hotel;
        this.numOfPeople= numOfPeople;
        this.numOfNights= numOfNights;
    }
    //

    public int bookingPrice(){
        return offer.getPrice()*numOfPeople*numOfNights;
    }

    @Override
    public String toString() {

        if (offer instanceof Trip){
            return "Booking{" +
                    "Trip=" + (Trip)offer +
                    ", numOfPeople=" + numOfPeople +
                    '}';
        }else if (offer instanceof  Hotel){
            return "Booking{" +
                    "Hotel=" + (Hotel)offer +
                    ", numOfPeople=" + numOfPeople +
                    ", numOfNights=" + numOfNights +
                    '}';
        }
        return null;
    }
}
