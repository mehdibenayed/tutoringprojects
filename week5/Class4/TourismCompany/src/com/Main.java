package com;

import com.model.Hotel;
import com.model.Trip;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {


        Trip trip1 = new Trip(1000, "Paris", 7);
        Trip trip2 = new Trip(2000, "Dubai", 10);
        Trip trip3 = new Trip(3000, "US", 9);

        Hotel hotel1 = new Hotel(50, "Paradise", 4);
        Hotel hotel2 = new Hotel(100, "The great hotel", 5);
        Hotel hotel3 = new Hotel(20, "Simple hotel", 3);

        Catalog catalog = new Catalog();

        catalog.addOffer(trip1);
        catalog.addOffer(trip2);
        catalog.addOffer(hotel1);
        catalog.addOffer(hotel2);
        catalog.addOffer(hotel3);
        catalog.addOffer(trip3);

        catalog.printAllOffers();
        catalog.printAllByPrice(1200);

        catalog.printHotelsByStars(4);
        catalog.printTripsByDuration(9);


        Booking booking1 = new Booking(trip3, 1);
        Booking booking2 = new Booking(trip1, 2);
        Booking booking3 = new Booking(hotel1, 4, 7);
        Booking booking4 = new Booking(hotel2, 1, 1);

        BookingStorage storage = new BookingStorage();
        storage.addBooking(booking1);
        storage.addBooking(booking2);
        storage.addBooking(booking3);
        storage.addBooking(booking4);

        storage.printAllBookings();

        System.out.format("The total income from all the bookings is %d euros", storage.totalIncome());
    }
}
