import model.*;

import java.util.ArrayList;
import java.util.Scanner;

public class Controller {

    private ArrayList<IAccount>accounts;
    private AccountFactory factory;
    private Account online;

    public Controller() {
        accounts = new ArrayList<>();
        this.factory = new AccountFactory();
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);
        createFirstAdmin(scanner);
        boolean stay = true;
        int choice;
        while (stay){
            printOnlineAccount();
            System.out.println("MAIN MENU: Please choose an option:");
            System.out.println("0:quit / 1:print / 2:creat an account / 3:delete account / 4:logout / 5:login");
            System.out.print("#Choice: ");
            choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice){
                case 0: stay = false; break;
                case 1: printAllAccounts();break;
                case 2: createAccount(scanner);break;
                case 3 : deleteAccount(scanner); break;
                case 4: online = null;break;//Logout
                case 5: login(scanner);
                default:
                    System.out.println("ERROR: choice a valid option!");
            }
        }
    }

    private void deleteAccount(Scanner scanner) {
        if (online instanceof Admin){
            printAllAccounts();
            System.out.print("#Please enter the ID of the account you would like to delete: ");
            int choice = scanner.nextInt();
            scanner.nextLine();
            int index = findIndexOFAccountByID(choice);
            if (index != -1){
                accounts.remove(index);
                System.out.println("Account was deleted!");
            }else {
                System.out.println("ERROR: Invalid ID!");
            }
        }else {
            System.out.println("ERROR: You should login as an admin first!");
        }
    }

    private int findIndexOFAccountByID(int id) {
        for (int i = 0 ; i<accounts.size() ; i++){
               if (accounts.get(i).getId() == id){
                   return i;
               }
        }
        return -1;
    }

    private void login(Scanner scanner) {
        System.out.print("#Please enter your username: ");
        String username = scanner.nextLine();
        if (usernameDosntExists(username)){
            System.out.println("ERROR: Username doesn't exist!");
        }else {
            System.out.print("#Please enter your password: ");
            String password = scanner.nextLine();
            Account found = findAccountByUsername(username);
            if (found.verifyPassword(password)){
                online= found;
            }else {
                System.out.println("ERROR: Password incorrect!");
            }
        }
    }

    private Account findAccountByUsername(String username) {
        for (IAccount a :accounts) {
            if (a.getUsername().equals(username)){
                return (Account) a;
            }
        }
        return null;
    }

    private void createAccount(Scanner scanner) {
        System.out.println("What type of account you would like to create?");
        System.out.println("1:Admin / 2:Customer / 0:back ");
        System.out.print("#Choice: ");
        int choice = scanner.nextInt();
        scanner.nextLine();
        switch (choice){
            case 0: break;
            case 1: createNewAdmin(scanner); break;
            case 2: createNewAccount(scanner, "customer"); break;
            default:
                System.out.println("ERROR: Invalid choice!");break;
        }
    }

    private void createNewAdmin(Scanner scanner) {
        if (online instanceof Admin){
            createNewAccount(scanner, "admin");
        }else {
            System.out.println("ERROR: You should login as an admin first!");
        }
    }

    private void createNewAccount(Scanner scanner, String type) {
        System.out.print("#Please enter a username: ");
        String username = scanner.nextLine();
        if (usernameDosntExists(username)){
            System.out.println("#Please enter password: ");
            String password = scanner.nextLine();
            if (type.equals("customer")){
                Customer customerCreated = factory.createnewCustomer(username,password);
                accounts.add(customerCreated);
            }else if (type.equals("admin")){
                Admin adminCreated = factory.createnewAdmin(username, password);
                accounts.add(adminCreated);
            }

        }else{
            System.out.println("ERROR: Username already exists!");
        }
    }

    private boolean usernameDosntExists(String username){
        for (IAccount a :accounts             ) {
            if (a.getUsername().equals(username)){
                return false;
            }
        }
        return true;
    }

    private void printOnlineAccount() {
        if (online != null){
            System.out.println("Onlice account : "+ online.getUsername());
        }else {
            System.out.println("No on is logged in.");
        }
    }

    private void printAllAccounts() {
        System.out.println();
        System.out.println("All accounts:");
        for (IAccount a:accounts             ) {
            System.out.println(a);
        }
    }

    private void createFirstAdmin(Scanner scanner) {
        System.out.println("Hey new admin");
        System.out.print("#Please enter your username: ");
        String username = scanner.nextLine();
        System.out.print("#Please enter your password: ");
        String password = scanner.nextLine();
        Admin adminCreated = factory.createnewAdmin(username,password);
        accounts.add(adminCreated);
        online= adminCreated;
    }
}
