package model;

public class AccountFactory {
    private int numberOfAccountsCreated;

    public AccountFactory(){
        numberOfAccountsCreated=0;
    }

    public Admin createnewAdmin(String username, String password){
        numberOfAccountsCreated++;
        return new Admin(numberOfAccountsCreated, username, password);
    }
    public Customer createnewCustomer(String username, String password){
        numberOfAccountsCreated++;
        return new Customer(numberOfAccountsCreated, username, password);
    }
}
