package model;

public interface IAccount {
    String getUsername();
    int getId();
}
