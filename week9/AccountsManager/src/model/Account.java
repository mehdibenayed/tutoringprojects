package model;

public abstract class Account implements IAccount {
    protected int id;
    protected String username;
    private String password;

    public Account(int id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", username='" + username + '\'' +
                '}';
    }

    public boolean verifyPassword(String password) {
        if (this.password.equals(password)){
            return true;
        }
        return false;
    }
}
