package model;

public class Customer extends Account{

    public Customer(int id, String username, String password) {
        super(id, username, password);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", username='" + username + '\'' +
                '}';
    }
}
